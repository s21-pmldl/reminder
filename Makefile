BOT_TAG ?= registry.gitlab.com/s21-pmldl/reminder/bot:local

.PHONY: bot
bot:
	export DOCKER_BUILDKIT=1 && docker build --tag ${BOT_TAG} --file docker/bot/Dockerfile . ${flags}

.PHONY: bot-no-cache
bot-no-cache:
	$(MAKE) bot flags='--no-cache'

.PHONY: run
run:
	docker-compose -f docker-compose.yml -p reminder up

.PHONY: connect
connect:
	ssh ubuntu@150.136.81.17

.PHONY: web
web:
	export DOCKER_BUILDKIT=1 && docker build --tag registry.gitlab.com/s21-pmldl/reminder/web:master --file docker/web/Dockerfile docker/web
	docker push registry.gitlab.com/s21-pmldl/reminder/web:master

# Install docker
# ---------------------------------------------------------------------------------------------------------------------
# curl -fsSL https://get.docker.com -o get-docker.sh
# sh get-docker.sh
# sudo usermod -aG docker ubuntu
# ---------------------------------------------------------------------------------------------------------------------

# Install runner
# ---------------------------------------------------------------------------------------------------------------------
# curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner
# ---------------------------------------------------------------------------------------------------------------------

# Register
# ---------------------------------------------------------------------------------------------------------------------
#