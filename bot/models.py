from django.db import models
from django.utils.translation import gettext as _

import django.utils.timezone


class Reminder(models.Model):
    class StatusChoices(models.TextChoices):
        RECEIVED = 'received', _('Получено')
        CREATED = 'created', _('Создано')
        SENT = 'sent', _('Отправлено')
        READ = 'read', _('Прочитано')

    chat_id = models.PositiveIntegerField(blank=True, null=True)
    message_id = models.PositiveIntegerField(blank=True, null=True)
    remind_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(default=django.utils.timezone.now)
    sent_at = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=10, blank=True, null=True)
    status = models.CharField(choices=StatusChoices.choices,
                              default=StatusChoices.CREATED,
                              max_length=10)
