import json

import requests
from datetime import timedelta, datetime
from django.conf import settings
from users.models import User
from bot.models import Reminder

import bot.tasks
from bot.tasks import BUTTON_TO_DELTA
from reminder import celery_app


class MessageHandler():
    def __init__(self, bot):
        self.bot = bot
        self.define_handlers()
        self.bot.polling(none_stop=True, interval=0)

    def define_handlers(self):
        @self.bot.callback_query_handler(func=lambda call: True)
        def callback_query(call):
            option, reminder_id = eval(call.data)
            reminder = Reminder.objects.get(id=reminder_id)

            task_id = bot.tasks.generate_reminder_and_save.apply_async(kwargs=dict(
                user_id=call.from_user.id,
                message_id=reminder.message_id,
                message_content=reminder.content,
                dt=datetime.now() + BUTTON_TO_DELTA[option]
            ))

        @celery_app.task
        @self.bot.message_handler(commands=['start'])
        def operate_start(message):
            tg_user = message.from_user
            user, is_created = User.register_tg_user(tg_user)

            if is_created:
                text = f'Привет, {user.first_name}! Добро пожаловать!'
            else:
                text = f'Привет, {user.first_name}! С возвращением :)'
            self.bot.reply_to(message=message, text=text)

        @self.bot.message_handler(content_types=['text'])
        def operate_text(message):
            task_id = bot.tasks.generate_reminder_and_save.apply_async(kwargs=dict(
                user_id=message.chat.id,
                message_id=message.message_id,
                message_content=message.text
            ))

        @self.bot.message_handler(content_types=['voice'])
        def operate_voice(message):
            reply = self.bot.reply_to(message=message, text='Мы начали распознавать звук')

            file_info = self.bot.get_file(message.voice.file_id)
            downloaded_file = self.bot.download_file(file_info.file_path)

            print(f'Sending S2T request: {settings.SPEECH_TO_TEXT_ADDRESS}')

            req = requests.post(
                settings.SPEECH_TO_TEXT_ADDRESS,
                files={'file': downloaded_file},
                headers={'Authorization': settings.MODEL_API_TOKEN}
            )
            text = req.content.decode('utf-8')

            if text == "":
                text = " "

            text = f'Вы хотите: "{text}"'
            self.bot.edit_message_text(text, message_id=reply.message_id, chat_id=reply.chat.id)

            task_id = bot.tasks.generate_reminder_and_save.apply_async(kwargs=dict(
                user_id=message.chat.id,
                message_id=message.message_id,
                message_content=text
            ))
