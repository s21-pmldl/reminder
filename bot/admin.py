from django.contrib import admin

from .models import Reminder


class ReminderAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'chat_id')
    list_filter = ('status', 'created_at')


admin.site.register(Reminder, ReminderAdmin)
