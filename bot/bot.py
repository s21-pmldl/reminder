from telebot import TeleBot

from django.conf import settings

bot_instance = TeleBot(token=getattr(settings, 'TELEGRAM_TOKEN'))
