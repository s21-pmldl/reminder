from django.conf import settings
from django.utils import autoreload
from django.core.management.base import BaseCommand

import bot
from bot.handlers import MessageHandler


class Command(BaseCommand):
    help = 'Bot starting command'

    def handle(self, *args, **options):
        self.stdout.write('Bot starts...')
        autoreload.run_with_reloader(self.run, args=None, kwargs=None)

    def run(self, *args, **kwargs):
        MessageHandler(bot=bot.bot_instance)
