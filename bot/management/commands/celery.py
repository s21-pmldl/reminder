import shlex
import subprocess

from django.core.management.base import BaseCommand
from django.utils import autoreload


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write('Starting celery worker with autoreload...')
        autoreload.run_with_reloader(Command.restart_celery, args=None, kwargs=None)

    @classmethod
    def restart_celery(*args, **kwargs):
        kill_worker_cmd = 'pkill -9 celery'
        subprocess.call(shlex.split(kill_worker_cmd))
        start_worker_cmd = 'celery -A reminder worker --beat --loglevel=info'
        subprocess.call(shlex.split(start_worker_cmd))
