from django.core.management import BaseCommand

from users.models import User


class Command(BaseCommand):
    def execute(self, *args, **options):
        if not User.objects.filter(email='admin@admin.ru').exists():
            User.objects.create_user(email='admin@admin.ru', password='admin')
