import json
import logging
from telebot import types
from babel.dates import format_datetime
from datetime import datetime, timedelta
from pytz import timezone

import bot
from users.models import User
from reminder import celery_app
from bot.models import Reminder
from tempinfo.extractor import extract_datetime_from_text

from django.conf import settings


def get_markup(reminder_id):
    markup = types.InlineKeyboardMarkup()
    buttons = [
        types.InlineKeyboardButton(
            text=option,
            callback_data=str((option, reminder_id))
        )
        for option in BUTTON_TO_DELTA
    ]
    markup.add(*buttons)
    return markup


BUTTON_TO_DELTA = {
    '⏰ 5 секунд': timedelta(seconds=5),
    '⏰ 1 минута': timedelta(minutes=1),
    '⏰ 15 минут': timedelta(minutes=15),
    '⏰ 1 час': timedelta(hours=1),
    '⏰ 2 часа': timedelta(hours=2),
    '⏰ Завтра': timedelta(days=1),
    '⏰ Послезавтра': timedelta(days=2),
    # 'Отложить'
}

logger = logging.getLogger(__name__)


@celery_app.task
def generate_reminder_and_save(user_id, message_content, message_id=None, dt=None):
    user = User.objects.get(external_id=user_id)

    if dt is not None and isinstance(dt, str):
        dt = datetime.fromisoformat(dt)
    else:
        # extract info from message using pipeline
        dt, _ = extract_datetime_from_text(message_content)

        if dt is None:
            print('could not recognize')
            reminder = Reminder.objects.create(
                chat_id=user.external_id,
                message_id=message_id,
                content=message_content,
                status=Reminder.StatusChoices.RECEIVED
            )

            bot.bot_instance.send_message(
                user.external_id,
                'Не смогли распознать. Выберите вручную',
                reply_markup=get_markup(reminder_id=reminder.id)
            )

            return

        dt_loc = timezone(settings.CELERY_TIMEZONE).localize(dt)
        dt_now = timezone(settings.CELERY_TIMEZONE).localize(datetime.now())
        if dt_loc < dt_now:
            print(f'reminder in past (?) [{dt_loc} < {dt_now}]')
            bot.bot_instance.send_message(
                user.external_id,
                f'Напоминание в прошлом [{dt_loc}] ? Неее, так не пойдет... Попробуем еще?',
            )

            return

    dt.replace(tzinfo=None)

    reminder, created = Reminder.objects.get_or_create(
        chat_id=user_id,
        message_id=message_id,
    )
    reminder.remind_at = dt
    reminder.status = Reminder.StatusChoices.CREATED
    if created:
        reminder.content = message_content
    reminder.save()

    reply = f'✅ Хорошо! Я напомню вам {format_datetime(dt, format="medium", locale="ru_RU")}:\n\n' \
            f'{reminder.content}\n'
    print(reply)

    bot.bot_instance.send_message(reminder.chat_id, reply, reply_to_message_id=reminder.message_id)

    send_reminder.apply_async(
        kwargs=dict(
            reminder_id=reminder.id
        ),
        eta=timezone(settings.CELERY_TIMEZONE).localize(reminder.remind_at)
    )


@celery_app.task
def send_reminder(reminder_id):
    try:
        reminder = Reminder.objects.get(id=reminder_id)
    except Reminder.DoesNotExist:
        logger.error('Reminder not found. Probably it was deleted')
        return
    bot.bot_instance.send_message(reminder.chat_id,
                                  '🔻' + reminder.content,
                                  reply_to_message_id=reminder.message_id)
    reminder.status = Reminder.StatusChoices.SENT
