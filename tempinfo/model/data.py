import torch
import numpy as np
import pandas as pd

from pprint import pprint
from typing import Optional, Union, List

from pymystem3 import Mystem
from torch.utils import data
from torchnlp.encoders import LabelEncoder
from torchnlp.encoders.text import pad_tensor
from transformers import AutoTokenizer, PreTrainedTokenizer

from tempinfo.entity import DayPoint, WeekPoint, WeekDay

mystem = Mystem()

torch.manual_seed(0)
np.random.seed(0)

NONE_TOKEN = '<NONE>'
DELTA_TOKEN = '<+>'
UNK_TOKEN = '<UNK>'
PAD_TOKEN = '<PAD>'

TARGET_FIELDS = ['year', 'month', 'week', 'weekday', 'day', 'daypoint', 'hour', 'minute']
FIELDS = TARGET_FIELDS + ['text']


def read_and_split(file, ratio=0.8):
    df = pd.read_csv(file, header=0, dtype=str)
    msk = np.random.rand(len(df)) < ratio
    train = df[msk]
    valid = df[~msk]
    return train, valid


class TIDataset(data.Dataset):
    PREFIXES = dict(
        UND=0,
        ABS=1,
        DELTA=2
    )

    PADDINGS = dict(
        year=4,
        month=2,
        week=2,
        day=3,
        hour=3,
        minute=3,
        text=20
    )

    PREFIXES_REVERSED = {v: k for k, v in PREFIXES.items()}
    SAMPLE_KEYS = [item for field in TARGET_FIELDS for item in [f'{field}_tokens', f'{field}_type']] + ['text_tokens']

    FIELD_TO_FUNCTION = dict(
        weekday='process_weekday',
        daypoint='process_daypoint',

        year='process_usual',
        month='process_usual',
        week='process_usual',
        day='process_usual',
        hour='process_usual',
        minute='process_usual',
    )

    assert sorted(TARGET_FIELDS) == sorted(list(FIELD_TO_FUNCTION.keys()))

    def get_vector_length(self, field: str) -> int:
        """
        Returns length of the field (for a single batch)
        """
        if field.endswith('type') or field.startswith('weekday') or field.startswith('daypoint'):
            return 1

        feature = field.split('_')[0]
        return TIDataset.PADDINGS[feature]

    def get_vocab_size(self, field: str) -> int:
        """
        Returns vocabulary of the field
        """
        assert field in TIDataset.SAMPLE_KEYS
        if field.endswith('type'):
            return len(TIDataset.PREFIXES)

        if field.startswith('text'):
            return self.encoder.vocab_size

        if field.startswith('weekday'):
            return len(WeekPoint) + len(WeekDay)

        if field.startswith('daypoint'):
            return len(DayPoint)

        return 10  # 0..9

    @staticmethod
    def get_und_mask(series: pd.Series):
        return series == NONE_TOKEN

    @staticmethod
    def get_delta_mask(series: pd.Series):
        return series.str.startswith(DELTA_TOKEN)

    @staticmethod
    def get_abs_mask(series: pd.Series):
        return ~TIDataset.get_und_mask(series) & ~TIDataset.get_delta_mask(series)

    @staticmethod
    def process_type(raw: pd.Series):
        und_mask = TIDataset.get_und_mask(raw)
        delta_mask = TIDataset.get_delta_mask(raw)

        pref = np.ones(len(raw), dtype=int)
        pref *= TIDataset.PREFIXES['ABS']
        pref[und_mask] = TIDataset.PREFIXES['UND']
        pref[delta_mask] = TIDataset.PREFIXES['DELTA']

        return pref

    @staticmethod
    def process_usual(raw: pd.Series, column_name: str):
        pad = TIDataset.PADDINGS[column_name]
        delta_mask = TIDataset.get_delta_mask(raw)
        raw[delta_mask] = raw[delta_mask].str.replace(DELTA_TOKEN, '', regex=False)

        raw.replace(NONE_TOKEN, '0' * pad, inplace=True)
        raw = raw.str.pad(pad, side='left', fillchar='0')
        raw = raw.apply(lambda x: [int(c) for c in x])

        return np.stack(raw, axis=0)

    @staticmethod
    def process_weekday(raw: pd.Series, column_name: str):
        def get_label(x):
            if x == NONE_TOKEN:
                return 0
            try:
                return WeekPoint.__getitem__(x).value
            except KeyError:
                return WeekDay.__getitem__(x).value

        res = raw.apply(
            lambda x: [get_label(x)]
        )
        return np.stack(res, axis=0)

    @staticmethod
    def process_daypoint(raw: pd.Series, column_name: str):
        res = raw.apply(
            lambda x: [0] if x == NONE_TOKEN
            else [DayPoint.__getitem__(x).value]
        )
        return np.stack(res, axis=0)

    def _huggingface_tokenize(self, raw, encoder: PreTrainedTokenizer):
        if encoder:
            self.encoder = encoder
        else:
            self.encoder = AutoTokenizer.from_pretrained('DeepPavlov/rubert-base-cased')
            self.encoder.add_tokens([NONE_TOKEN])

        tokenized = self.encoder(
            list(raw.values),
            padding='max_length',
            max_length=TIDataset.PADDINGS['text'],
            return_tensors='pt'
        )['input_ids']
        return tokenized

    def _torchnlp_labelencode(self, raw, encoder: LabelEncoder):
        samples = raw.apply(
            lambda x: ''.join(mystem.lemmatize(x)[:-1])
        )
        samples = samples.str.split(r'([^a-zA-Zа-яА-Я])')
        samples = samples.apply(
            lambda x: [token for token in x if token and token[0] != ' ']
        )

        if encoder is not None:
            self.encoder = encoder
        else:
            words = samples.apply(pd.Series).stack()
            self.encoder = LabelEncoder(words, reserved_labels=[PAD_TOKEN, UNK_TOKEN], unknown_index=1)

        labeled = samples.apply(
            lambda x:
            pad_tensor(
                self.encoder.batch_encode(x),
                TIDataset.PADDINGS['text']
            )
        )

        return labeled.values

    def process_text(self, raw: pd.Series, encoder):
        if self.tokenization_type == 'huggingface':
            return self._huggingface_tokenize(raw, encoder)
        return self._torchnlp_labelencode(raw, encoder)

    def get_distribution(self, field: str):
        assert field.endswith('_type')
        values = getattr(self, field)
        stat = torch.bincount(values, minlength=3)
        return stat

    def decode_text(self, data: torch.Tensor, unpad: bool = True):
        """
        Decodes text into words. Data is either batch or sample
        """

        def decode_labeling():
            def decode_sample(sample):
                if unpad:
                    return [x for x in encoder.batch_decode(sample) if x != PAD_TOKEN]
                return encoder.batch_decode(sample)

            encoder: LabelEncoder = self.encoder
            if len(data.shape) == 1:
                return decode_sample(data)
            return [decode_sample(sample) for sample in data]

        if self.tokenization_type == 'huggingface':
            raise NotImplementedError('Detokenization for huggingface is not implemented yet')
        return decode_labeling()

    def decode_targets(self, data: dict) -> List[dict]:
        def map_types(feature):
            return TIDataset.PREFIXES_REVERSED[int(feature)]

        def join_digits(feature):
            return int(''.join([str(x) for x in feature]))

        results = list()
        samples = [
            {k: v[i] for k, v in data.items()}
            for i in range(len(data['text_tokens']))
        ]
        for sample in samples:
            result = dict()
            result['text_tokens'] = self.decode_text(sample['text_tokens'])

            for target in TARGET_FIELDS:
                target_type, target_tokens = target + '_type', target + '_tokens'
                sample[target_type] = sample[target_type].cpu().detach().numpy()
                sample[target_tokens] = sample[target_tokens].cpu().detach().numpy()

                result[target_type] = map_types(sample[target_type])
                if result[target_type] == 'UND':
                    result[target_tokens] = None
                elif target == 'weekday':
                    try:
                        result[target_tokens] = WeekPoint(sample[target_tokens])
                    except ValueError:
                        result[target_tokens] = WeekDay(sample[target_tokens])
                elif target == 'daypoint':
                    result[target_tokens] = DayPoint(sample[target_tokens])
                else:
                    result[target_tokens] = join_digits(sample[target_tokens])
            pprint(result)
            results.append(result)

        return results

    def __init__(self, df: pd.DataFrame,
                 encoder_type: str,
                 encoder: Union[LabelEncoder, PreTrainedTokenizer, None] = None):
        # TODO lemmatize only for non-numeric words (month, week, etc)
        """
        df: input dataframe
        encoder_type: one of the 'huggingface', 'labeling'
        encoder (Optional): torch-nlp LabelEncoder object or HuggingFace Tokenizer object.
                            Pass only for validation/test splits
        """
        assert encoder_type in ['huggingface', 'labeling']
        self.tokenization_type = encoder_type
        self.text_tokens = self.process_text(df['text'].astype(str), encoder=encoder)

        self.df = df.fillna(value=NONE_TOKEN)
        for field in TARGET_FIELDS:
            function_name = TIDataset.FIELD_TO_FUNCTION[field]
            function = getattr(TIDataset, function_name)
            field_tokens = function(self.df[field].astype(str), field)
            setattr(self, f'{field}_tokens', torch.tensor(field_tokens))

            field_type = TIDataset.process_type(self.df[field].astype(str))
            setattr(self, f'{field}_type', torch.LongTensor(field_type))

    def __len__(self):
        return len(self.__getattribute__('text_tokens'))

    def __getitem__(self, idx):
        item = {
            key: self.__getattribute__(key)[idx]
            for key in TIDataset.SAMPLE_KEYS
        }
        return item


if __name__ == '__main__':
    data_file = '/home/sabzirov/Desktop/Spring21/reminder/tempinfo/dataset/TI_dataset_1.csv'

    train_df, valid_df = read_and_split(data_file)
    enc_type = 'labeling'
    train_ds = TIDataset(train_df, encoder_type=enc_type)
    valid_ds = TIDataset(valid_df, encoder_type=enc_type, encoder=train_ds.encoder)

    train_dataloader = data.DataLoader(train_ds, batch_size=2)
    batch = next(iter(train_dataloader))
    pprint(batch)
    pprint(train_ds.decode_targets(batch))

    for field in TARGET_FIELDS:
        field = field + '_type'
        print(field, train_ds.get_vocab_size(field), train_ds.get_distribution(field))

    for field, value in batch.items():
        _ = f'field: {field}, vocab_size: {train_ds.get_vocab_size(field)}, length: {train_ds.get_vector_length(field)}'
        print(_)
    print(train_ds.decode_text(batch['text_tokens']))

    # torch.save({'train': train_ds, 'valid': valid_ds}, 'dataset.pt')
