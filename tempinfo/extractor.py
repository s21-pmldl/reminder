import json
from datetime import datetime

import dateparser
import dateparser.search as search
import pytz
import requests
from django.conf import settings


def extract_datetime_from_text(text: str):
    print(f'Received text: {text}')
    req = requests.post(
        settings.DPQA_ADDRESS,
        json={'text': text},
        headers={'Authorization': settings.MODEL_API_TOKEN}
    )
    resp = json.loads(req.content)
    print(f'DPQA: {resp}')

    # fixme: "Напомни " in the start of what
    when, what = resp['when'], resp['what']

    date = dateparser.parse(when)

    print(f'Extracted | date: {date} | what: {what}')

    return date, what


if __name__ == '__main__':
    extract_datetime_from_text("завтра ")
    assert extract_datetime_from_text("предыдущий понедельник") != extract_datetime_from_text("следующий понедельник")
