import sys

from datetime import datetime, timedelta
from typing import Type
import pandas as pd
import enum
import unittest


@enum.unique
class Month(enum.IntEnum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12


class WeekDay(enum.IntEnum):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6


class DayPoint(enum.IntEnum):
    MORNING = enum.auto()
    AFTERNOON = enum.auto()
    EVENING = enum.auto()
    NIGHT = enum.auto()
    NOON = enum.auto()
    MIDNIGHT = enum.auto()


class WeekPoint(enum.IntEnum):
    START = 7
    MIDDLE = 8
    END = 9
    WEEKEND = 10


class RelativeDay(enum.IntEnum):
    TOMORROW = +1
    AFTER_TOMORROW = +2


class Preferences:
    default = {
        WeekPoint: {
            WeekPoint.START: WeekDay.MONDAY,
            WeekPoint.MIDDLE: WeekDay.WEDNESDAY,
            WeekPoint.END: WeekDay.SUNDAY,
            WeekPoint.WEEKEND: WeekDay.SATURDAY,
        },
        DayPoint: {
            DayPoint.MORNING: dict(hour=8, minute=0),
            DayPoint.AFTERNOON: dict(hour=12, minute=0),
            DayPoint.EVENING: dict(hour=18, minute=0),
            DayPoint.NIGHT: dict(hour=0, minute=0),
        }
    }

    def __init__(self, preferences=None):
        self.preferences = self.default.copy()

        if preferences is not None:
            self.preferences.update(preferences)

    def __getitem__(self, option):
        return self.preferences[type(option)][option]


def _is_enum(val) -> bool:
    return issubclass(type(val), enum.Enum)


def _just_num(val) -> bool:
    return isinstance(val, int) and not _is_enum(val)


class Entity:
    def __init__(self, year=None, month=None, week=None, day=None, hour=None, minute=None):
        self.year = year
        self.month = month
        self.week = week
        self.day = day
        self.hour = hour
        self.minute = minute

        if not self.validate():
            raise ValueError(f'invalid declaration: {self}')

    def __repr__(self):
        def _day(day):
            if isinstance(day, WeekDay):
                return self.day.name
            if isinstance(day, WeekPoint):
                return f'WEEK_{self.day.name}'
            if isinstance(self.day, int):
                return self.day

        repr = []
        if self.year is not None:
            repr.append(f'Y:{self.year}')
        if self.month is not None:
            repr.append(f'M:{self.month.name}')
        if self.week is not None:
            repr.append(f'W:{self.week}')
        if self.day is not None:
            repr.append(f'D:{_day(self.day)}')
        if self.hour is not None:
            repr.append(f'H:{self.hour}')
        if self.minute is not None:
            repr.append(f'm:{self.minute}')

        return '  '.join(repr)

    # @staticmethod
    # def from_date(date: datetime.date, mask=None):
    #     if mask is None:
    #         return Entity(year=date.year, month=date.month, day=date.day)
    #
    #     year = date.year if 'Y' in mask else None
    #     year = date.year if 'Y' in mask else None
    #     year = date.year if 'Y' in mask else None
    #
    #     return Entity(year=year, month=date.month, day=date.day)

    def validate(self):
        # AND -> OR -> conds
        conditions = [
            all(o is None for o in [self.year, self.month]) if isinstance(self.day, RelativeDay) else True,
            all(o is None for o in [self.year, self.month]) if isinstance(self.day, WeekDay) else True,
        ]

        return all(conditions)

    def datetime(self, current_dt: datetime) -> datetime:
        dt = current_dt

        if isinstance(self.day, WeekDay):
            day_shift = (self.day - current_dt.weekday()) % 7
            if day_shift == 0:
                day_shift = 7
            dt += timedelta(days=day_shift)

        if isinstance(self.day, RelativeDay):
            dt += timedelta(days=self.day.value)

        year = self.year if self.year is not None else dt.year
        month = self.month if self.month is not None else dt.month
        day = self.day if _just_num(self.day) else dt.day
        hour = self.hour if _just_num(self.hour) else dt.hour
        minute = self.minute if _just_num(self.minute) else dt.minute

        if hour < current_dt.hour and day == current_dt.day and month == current_dt.month and year == current_dt.year:
            day += 1
        if minute < current_dt.minute and hour < current_dt.hour and day == current_dt.day and month == current_dt.month and year == current_dt.year:
            hour += 1

        return datetime(year=year, month=month, day=day, hour=hour, minute=minute)


class EntityTest(unittest.TestCase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.current_dt = datetime(2021, 2, 20, 12, 16)

    def test_validate(self):
        Entity(month=Month.FEBRUARY, day=23)
        Entity(day=WeekDay.MONDAY)

        with self.assertRaises(ValueError):
            Entity(month=Month.FEBRUARY, day=WeekDay.MONDAY)

    def test1(self):
        # завтра днем
        prefs = Preferences()
        time = prefs[DayPoint.AFTERNOON]
        entity = Entity(day=RelativeDay.TOMORROW, hour=time['hour'], minute=time['minute'])
        dt = entity.datetime(self.current_dt)
        print(dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')
        self.assertEqual(dt.hour, 12, msg=f'{dt}')

    def test2(self):
        # FEB 23
        entity = Entity(month=Month.FEBRUARY, day=23)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 23, msg=f'{dt}')

    def test3(self):
        # 2023 JUN 3
        entity = Entity(year=2023, month=Month.JUNE, day=3)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2023, msg=f'{dt}')
        self.assertEqual(dt.month, Month.JUNE, msg=f'{dt}')
        self.assertEqual(dt.day, 3, msg=f'{dt}')

    def test4(self):
        # MON
        entity = Entity(day=WeekDay.MONDAY)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.weekday(), WeekDay.MONDAY)
        self.assertEqual(dt.day, 22, msg=f'{dt}')

    def test5(self):
        # WE [SAT]
        prefs = Preferences()
        entity = Entity(day=prefs[WeekPoint.WEEKEND])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.weekday(), WeekDay.SATURDAY)
        self.assertEqual(dt.day, 27, msg=f'{dt}')

    def test6(self):
        # в начале недели
        prefs = Preferences()
        entity = Entity(day=prefs[WeekPoint.START])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.weekday(), WeekDay.MONDAY)
        self.assertEqual(dt.day, 22, msg=f'{dt}')

    def test7(self):
        # в середине недели
        prefs = Preferences()
        entity = Entity(day=prefs[WeekPoint.MIDDLE])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.weekday(), WeekDay.WEDNESDAY)
        self.assertEqual(dt.day, 24, msg=f'{dt}')

    def test8(self):
        # в конце недели
        prefs = Preferences()
        entity = Entity(day=prefs[WeekPoint.END])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.weekday(), WeekDay.SUNDAY)
        self.assertEqual(dt.day, 21, msg=f'{dt}')

    def test9(self):
        # завтра
        entity = Entity(day=RelativeDay.TOMORROW)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')

    def test10(self):
        # послезавтра
        entity = Entity(day=RelativeDay.AFTER_TOMORROW)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 22, msg=f'{dt}')

    def test11(self):
        # 22го
        entity = Entity(day=RelativeDay.AFTER_TOMORROW)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 22, msg=f'{dt}')

    def test12(self):
        # завтра утром
        prefs = Preferences()
        time = prefs[DayPoint.MORNING]
        entity = Entity(day=RelativeDay.TOMORROW, hour=time['hour'], minute=time['minute'])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')
        self.assertEqual(dt.hour, 8, msg=f'{dt}')

    def test13(self):
        # завтра вечером
        prefs = Preferences()
        time = prefs[DayPoint.EVENING]
        entity = Entity(day=RelativeDay.TOMORROW, hour=time['hour'], minute=time['minute'])
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')
        self.assertEqual(dt.hour, 18, msg=f'{dt}')

    def test14(self):
        # в полночь
        entity = Entity(hour=0, minute=0)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')
        self.assertEqual(dt.hour, 0, msg=f'{dt}')

    def test15(self):
        # в 6 часов 30 минут
        entity = Entity(hour=6, minute=38)
        dt = entity.datetime(self.current_dt)
        self.assertEqual(dt.year, 2021, msg=f'{dt}')
        self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        self.assertEqual(dt.day, 21, msg=f'{dt}')
        self.assertEqual(dt.hour, 6, msg=f'{dt}')
        self.assertEqual(dt.minute, 38, msg=f'{dt}')

        # <eve> 6
        # entity = Entity(hour=6, minute=38)
        # dt = entity.datetime(current_dt)
        # self.assertEqual(dt.year, 2021, msg=f'{dt}')
        # self.assertEqual(dt.month, Month.FEBRUARY, msg=f'{dt}')
        # self.assertEqual(dt.day, 21, msg=f'{dt}')
        # self.assertEqual(dt.hour, 6, msg=f'{dt}')
        # self.assertEqual(dt.minute, 38, msg=f'{dt}')

        # <mor>10
        # Relative Datetime
        #
        # sample
        # Year
        # month
        # week
        # day
        # hour
        # mins
        # через 5 дней
        #
        #
        #
        #
        #
        #
        # <+>5
        #
        #
        #
        #
        # через неделю
        #
        #
        #
        #
        # <+>1
        #
        #
        #
        #
        #
        #
        # через 8 недель
        #
        #
        #
        #
        # <+>8
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        # через 2 часа
        #
        #
        #
        #
        #
        #
        #
        #
        # <+>2
        #
        #
        # через два с половиной часа
        #
        #
        #
        #
        #
        #
        #
        #
        # <+>2
        # <+>30
        # через два с четвертью часа
        #
        #
        #
        #
        #
        #
        #
        #
        # <+>2
        # <+>15
        # через 30 минут
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        # <+>30
        # через 2 часа 30 минут
        #
        #
        #
        #
        #
        #
        #
        #
        # <+>2
        # <+>30
        # any nullable datetime
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        #
        # Relative date
        #
        #
        #     pass


if __name__ == '__main__':
    unittest.main()
