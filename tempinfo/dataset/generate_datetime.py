import random
import logging
import itertools
from faker import Faker
from babel.dates import *

from utils import *
from utils import Format as _
from datetime_formats import *

logger = logging.Logger(__name__)

fake = Faker()
Faker.seed(12345)
random.seed(12345)

LOCALE = 'ru_RU'

DATETIME_FORMATS = list(itertools.product(DATE_FORMATS, TIME_FORMATS))
DATETIME_FORMATS = [_.combine(obj[0], obj[1]) for obj in DATETIME_FORMATS]

CONFIGS = [
    {
        'formats': TIME_FORMATS,
        'gen_function': fake.time_object,
        'gen_args': {},
        'format_function': format_time,
    },
    {
        'formats': DATE_FORMATS,
        'gen_function': fake.future_date,
        'gen_args': {'end_date': '+365d'},
        'format_function': format_date,
    },
    {
        'formats': DATETIME_FORMATS,
        'gen_function': fake.future_datetime,
        'gen_args': {'end_date': '+365d'},
        'format_function': format_datetime,
    },
]


def generate_object(faker_function, babel_function, format_: _, faker_args=None):
    if faker_args is None:
        faker_args = dict()

    obj = faker_function(**faker_args)
    try:
        human = babel_function(
            obj,
            format_.format,
            locale=LOCALE
        )
        human = human.lower()
        machine = obj.isoformat()
    except AttributeError as e:
        print(e)
        return None, None, None

    if PATTERN_ENTITY_TO_CHARS['period'] in format_.format:  # period
        period_id = get_period_id(obj)
        if format_.is_h_in():
            period = DAY_PERIODS['connected'][period_id]
        else:
            period = DAY_PERIODS['stand-alone'][period_id]

        format_.defined['daypoint'] = period_id.upper()[:-1] if period_id[-1] == '1' else period_id.upper()

        human = human.replace('am', period)
        human = human.replace('pm', period)

    yield human, machine, obj

    human_spelled = spell_numbers(human, format_)

    if human != human_spelled:
        yield human_spelled, machine, obj


def generate(count=1, print_samples=False):
    samples_list = []

    def construct_sample(human_readable, python_obj, format):
        sample = entity_dict(None)
        sample.update(format.defined)

        for char in format:
            if char not in PATTERN_CHARS_TO_ENTITY or char in PATTERN_ENTITY_TO_CHARS['period']:
                continue
            entity = PATTERN_CHARS_TO_ENTITY[char]

            value = getattr(python_obj, entity)
            if not format.is_ignored(entity):
                sample[entity] = value

        if format.is_h_in_0_11():
            sample['hour'] %= 12
        if format.is_h_in_1_12():
            sample['hour'] %= 12

        if format.is_h_in() and sample['minute'] is None:
            sample['minute'] = 0

        sample['text'] = human_readable
        sample = dict([(k, v) if v is None else (k, str(v)) for k, v in sample.items()])
        samples_list.append(sample)
        return sample

    for config in CONFIGS:
        formats = config.get('formats')
        gen_function = config.get('gen_function')
        gen_args = config.get('gen_args')
        format_function = config.get('format_function')

        for fmt in formats:
            for i in range(count):
                generator = generate_object(faker_function=gen_function,
                                            babel_function=format_function,
                                            format_=fmt,
                                            faker_args=gen_args)

                for human_readable, machine_readable, python_obj in generator:
                    sample = construct_sample(human_readable, python_obj, fmt)
                    if print_samples:
                        print('sample', {k: v for k, v in sample.items() if v is not None}, fmt, '\n')

                if not is_variable_format(fmt):  # otherwise it generates exactly the same result
                    break

    return samples_list


if __name__ == '__main__':
    samples = generate(print_samples=True)
