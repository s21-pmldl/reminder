import pandas as pd
import generate_datetime
import generate_timedelta

samples = generate_datetime.generate(count=1) + \
          generate_timedelta.generate()

df = pd.DataFrame.from_records(samples)
df.to_csv('TI_dataset_1.csv')

samples += generate_datetime.generate(count=99)
df = pd.DataFrame.from_records(samples)
df.to_csv('TI_dataset_100.csv')
