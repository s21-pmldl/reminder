from utils import Format as _
from tempinfo.entity import WeekDay, DayPoint, WeekPoint, RelativeDay

FUTURE_TOKEN = '<+>'

# Formats from http://babel.pocoo.org/en/latest/dates.html#date-fields
DATE_FORMATS = [
    _('d.MM.YY'), _('dd.MM.YYYY'), _('d MMMM YYYY'),
    _('dго MMMM YYYY'), _('d-го MMMM YYYY'), _('d го MMMM YYYY'),
    _('dго MMMM YYYY года'), _('d-го MMMM YYYY года'), _('d го MMMM YYYY года'),

    _('d.MM'), _('dd.MM'), _('d MMMM'),
    _('dго MMMM'), _('d-го MMMM'), _('d го MMMM'),

    #_('dго'), _('d-го'), _('d го'),

    _('в понедельник', ignore_date=True, defined=dict(weekday=WeekDay.MONDAY.name)),
    _('во вторник', ignore_date=True, defined=dict(weekday=WeekDay.TUESDAY.name)),
    _('в среду', ignore_date=True, defined=dict(weekday=WeekDay.WEDNESDAY.name)),
    _('в четверг', ignore_date=True, defined=dict(weekday=WeekDay.THURSDAY.name)),
    _('в пятницу', ignore_date=True, defined=dict(weekday=WeekDay.FRIDAY.name)),
    _('в субботу', ignore_date=True, defined=dict(weekday=WeekDay.SATURDAY.name)),
    _('в воскресенье', ignore_date=True, defined=dict(weekday=WeekDay.SUNDAY.name)),

    _('завтра', ignore_date=True, defined=dict(day=f'{FUTURE_TOKEN}1')),  # TODO not sure
    _('послезавтра', ignore_date=True, defined=dict(day=f'{FUTURE_TOKEN}2')),  # TODO not sure

    _('в начале недели', ignore_date=True, defined=dict(weekday=WeekPoint.START.name)),
    _('в середине недели', ignore_date=True, defined=dict(weekday=WeekPoint.MIDDLE.name)),
    _('на выходных', ignore_date=True, defined=dict(weekday=WeekPoint.WEEKEND.name)),
    _('в конце недели', ignore_date=True, defined=dict(weekday=WeekPoint.END.name)),
]

TIME_FORMATS = [
    _('H:mm'), _('в H:mm'),
    _('в H.mm'), _('в H-mm'),
    _('в H часов m минут'), _('в H часа m минут'),
    _('в H'), _('в H часов'),

    _('в H a'), _('в h a'),  # в H утра/дня/вечера/ночи
    _('в H:mm a'), _('в h:mm a'),

    _('утром', ignore_time=True, defined=dict(daypoint=DayPoint.MORNING.name)),
    _('днем', ignore_time=True, defined=dict(daypoint=DayPoint.AFTERNOON.name)),
    _('вечером', ignore_time=True, defined=dict(daypoint=DayPoint.EVENING.name)),
    _('ночью', ignore_time=True, defined=dict(daypoint=DayPoint.NIGHT.name)),
    _('в полдень', ignore_time=True, defined=dict(hour=12, minute=0)),
    _('в полночь', ignore_time=True, defined=dict(hour=0, minute=0))
]
