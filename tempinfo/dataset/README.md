# Dataset generation

List of possible test cases: [link](https://docs.google.com/document/d/1es1nBahk2-B3Mgul1x5kVf-dQtkwSV8CBG8XzSXt4_8/edit?usp=sharing)

Consist of generation of two types:
- datetime
- timedelta

To generate dataset: 
1. Configure `COUNT` in generate_datetime.py
1. Configure `WEEKS`, `DAYS`, `HOURS`, `MINUTES` in generate_timedelta.py
1. Run main.py