from utils import entity_dict, spell_numbers
from datetime_formats import FUTURE_TOKEN

from itertools import product
from datetime import timedelta
from babel.dates import format_timedelta

LOCALE = 'ru_RU'
AFTER = 'через'

YEARS = [None, 1, 2, 3]
MONTHS = [None, 1, 2, 3, 4, 6, 12, 18, 24]
WEEKS = [None, 1, 2, 3, 4, 5, 6, 7, 8, 12]
DAYS = [None, 1, 2, 3, 4, 5, 6, 10, 15, 30]
HOURS = [None, 1, 2, 3, 5, 10, 24, 48, 72]
MINUTES = [None, 5, 10, 15, 30, 45, 60, 90, 120]


def generate(print_samples=False):
    def mult(a, b: int):
        if a is None:
            return None
        return a * b

    samples_list = list()

    for sample in product(YEARS, MONTHS, WEEKS, DAYS, HOURS, MINUTES):
        if not 1 <= len(sample) - sample.count(None) <= 3:
            # There should be from 1 to 3 non-None elements
            continue

        years, months, weeks, days, hours, minutes = sample
        args = [
            ('year', years, {'days': mult(years, 365)}),
            ('month', months, {'days': mult(months, 30)}),
            ('week', weeks, {'weeks': weeks}),
            ('day', days, {'days': days}),
            ('hour', hours, {'hours': hours}),
            ('minute', minutes, {'minutes': minutes})
        ]
        text = AFTER + ' '

        for arg_name, arg_value, delta_args in args:
            if arg_value is None:
                continue
            delta = timedelta(**delta_args)
            formatted = format_timedelta(delta,
                                         format='long',
                                         locale=LOCALE,
                                         granularity=arg_name,
                                         threshold=10 ** 9,
                                         add_direction=True)
            formatted = formatted[6:]
            text = text + formatted + ' '
        text = text.strip()

        args = dict([
            (key, value) if value is None else (key, f'{FUTURE_TOKEN}{value}')
            for key, value, _ in args
        ])

        for txt in (text, spell_numbers(text, text, remove_end=False)):
            sample = entity_dict(None)
            sample.update(args)
            sample['text'] = txt

            samples_list.append(sample)

            if print_samples:
                print('sample', {k: v for k, v in sample.items() if v is not None}, args, '\n')

    return samples_list


if __name__ == '__main__':
    samples = generate(print_samples=True)
