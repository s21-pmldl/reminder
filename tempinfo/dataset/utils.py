import re
from num2words import num2words

TOKEN_SPLIT_PATTERN = r'[^.,?!\:\-\s]+|[.,?!:\-\s]'

PATTERN_ENTITY_TO_CHARS = {  # adapted from babel.dates.PATTERN_CHARS
    'era': 'G',
    'year': 'yYu',
    'quarter': 'Qq',
    'month': 'ML',
    'week': 'wW',
    'day': 'dDFG',
    'weekday': 'Eec',
    'period': 'a',
    'hour': 'hHKk',
    'minute': 'm',
    'second': 'sSA',
    'zone': 'zZOvVxX',
}
PATTERN_CHARS_TO_ENTITY = {}
for entity, chars in PATTERN_ENTITY_TO_CHARS.items():
    for char in chars:
        PATTERN_CHARS_TO_ENTITY[char] = entity

DAY_PERIODS = {
    'connected': {
        'afternoon1': 'дня',
        'evening1': 'вечера',
        'midnight': 'полночь',
        'morning1': 'утра',
        'night1': 'ночи',
        'noon': 'полдень',
    },
    'stand-alone': {
        'afternoon1': 'днем',
        'evening1': 'вечером',
        'midnight': 'в полночь',
        'morning1': 'утром',
        'night1': 'ночью',
        'noon': 'в полдень',
    }
}

ENTITIES = ['year', 'month', 'week', 'weekday', 'day', 'daypoint', 'hour', 'minute']


def entity_dict(values=None):
    if callable(values):
        return dict([
            (ent, values()) for ent in ENTITIES
        ])
    else:
        return dict([
            (ent, values) for ent in ENTITIES
        ])


class Format:
    def __init__(self, format: str, ignore_date: bool = False, ignore_time: bool = False, defined=None):
        if defined is None:
            defined = dict()
        self.format = format
        self.ignore_date = ignore_date
        self.ignore_time = ignore_time
        self.defined = defined

    @staticmethod
    def combine(first, second):
        assert isinstance(first, Format)
        assert isinstance(second, Format)
        return Format(
            format=f'{first.format} {second.format}',
            ignore_date=first.ignore_date or second.ignore_date,
            ignore_time=first.ignore_time or second.ignore_time,
            defined={**first.defined, **second.defined},
        )

    def is_ignored(self, entity):
        if self.ignore_date and entity in ['year', 'month', 'week', 'weekday', 'day']:
            return True
        if self.ignore_time and entity in ['hour', 'minute']:
            return True
        return False

    def is_h_in(self):
        return 'h' in self.format or 'H' in self.format or 'K' in self.format or 'k' in self.format

    def is_h_in_1_12(self):
        return 'h' in self.format

    def is_h_in_0_11(self):
        return 'K' in self.format

    def __iter__(self):
        return iter(self.format)

    def __str__(self):
        return f'Format: <`{self.format}`, defined={self.defined}, ignore=[{self.ignore_date}, {self.ignore_time}]>'

    def __repr__(self):
        return self.__str__()


def spell_numbers(human, format_, remove_end=True):
    def remove_endings(s):
        s = s.replace('-го', 'го')
        s = s.replace(' го', 'го')
        s = s.replace('го', '')  # TODO not sure
        return s

    if isinstance(format_, Format):
        format_ = format_.format

    res = []
    if remove_end:
        human = remove_endings(human)
        format_ = remove_endings(format_)

    matches_human = re.finditer(TOKEN_SPLIT_PATTERN, human)
    matches_format = re.finditer(TOKEN_SPLIT_PATTERN, format_)
    for match_human, match_format in zip(matches_human, matches_format):
        token_human = match_human.group()
        token_format = match_format.group()
        try:
            integer = int(token_human)

            if token_format in ['d', 'dd', 'MM', 'YY', 'YYYY']:
                word = num2words(integer, lang='ru', to='ordinal')
                if integer % 10 == 3:
                    word = word[:-2] + 'ьего'
                else:
                    word = word[:-2] + 'ого'
            else:
                word = num2words(integer, lang='ru', to='cardinal')

            word = word.lower()
            res.append(word)
        except ValueError as ve:
            res.append(token_human)

    human_textual = ''.join(res)
    return human_textual


def is_variable_format(format_):
    for char in PATTERN_CHARS_TO_ENTITY.keys():
        if char in format_:
            return True
    return False
