from django.db import models
from django.utils.translation import gettext as _
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)  # do not have access to internal admin
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Суперпользователь должен иметь is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Суперпользователь должен иметь is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    User: bot user
    """
    # TODO validate that all users have external_id (except admins)
    external_id = models.PositiveIntegerField(unique=True, blank=True, null=True)

    email = models.EmailField(_('email address'), unique=True, blank=True, null=True)

    first_name = models.CharField(_('first name'), max_length=30, blank=False, null=True, default="")
    last_name = models.CharField(_('last name'), max_length=30, blank=False, null=True, default="")

    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_(
                                        'Designates whether this user account should be considered active. '
                                        'It is recommended to set this flag to False instead of deleting accounts'))
    is_staff = models.BooleanField(_('is stuff'), default=False,
                                   help_text=_('Staff of development company'))
    is_admin = models.BooleanField(_('is admin'), default=False,
                                   help_text=_('Admin'))

    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = f'{self.first_name} {self.last_name}'
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    @staticmethod
    def register_tg_user(tg_user):
        return User.objects.get_or_create(
            external_id=tg_user.id,
            defaults=dict(
                first_name=tg_user.first_name,
                last_name=tg_user.last_name,
            )
        )

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        indexes = [
            models.Index(fields=['email'])
        ]
